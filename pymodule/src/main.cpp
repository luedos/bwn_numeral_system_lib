#include <pybind11/pybind11.h>
#include "numeralSystem.hpp"

namespace py = pybind11;

PYBIND11_MODULE(py_numeral_system, m) 
{
    py::class_<bwn::NumeralSystem>(m, "NumeralSystem")
        .def(py::init<int32_t>(), py::arg("base"))
        .def(py::init<int32_t, std::int64_t>(), py::arg("base"), py::arg("baseTenNumber"))

        .def("get", [](const bwn::NumeralSystem& self, std::size_t id) -> int { return self[id]; })
        .def("size", [](const bwn::NumeralSystem& self) -> std::size_t { return self.GetLength(); })
        .def("get_base", [](const bwn::NumeralSystem& self) -> int32_t { return self.GetBase(); })
        .def("copy", [](const bwn::NumeralSystem& self) -> bwn::NumeralSystem { return bwn::NumeralSystem{ self }; })
        .def("set_base", [](bwn::NumeralSystem& self, int32_t base) { return self.ChangeBase(base); })
        .def(
            "str", 
            [](const bwn::NumeralSystem& self) -> std::string 
            { 
                std::string ret;

                const std::size_t size = self.GetLength();

                {
                    std::size_t i = 0;
                    for (Digit localI = 1; localI < self.GetBase(); localI *= 10, ++i);

                    ret.reserve(size * (i + 1));
                }

                for (int32_t i = size - 1; i >= 0; --i)
                {
                    ret.append(std::to_string(self[i]));
                    ret.push_back('|');
                }
                ret.pop_back();

                return ret;
            })

        .def("add", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) { self += other; })
        .def("sub", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) { self -= other; })
        .def("mul", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) { self *= other; })
        .def("div", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) { self /= other; })

        .def("compare_eq", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) -> bool { return self == other; })
        .def("compare_neq", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) -> bool { return self != other; })
        .def("compare_l", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) -> bool { return self < other; })
        .def("compare_le", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) -> bool { return self <= other; })
        .def("compare_g", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) -> bool { return self > other; })
        .def("compare_ge", [](bwn::NumeralSystem& self, const bwn::NumeralSystem& other) -> bool { return self >= other; });


#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}
